package com.bitcoin.rate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtcRatesApplication {

	/**
	 * Application entry point.
	 */
	public static void main(String[] args) {
		SpringApplication.run(BtcRatesApplication.class, args);
	}
}
