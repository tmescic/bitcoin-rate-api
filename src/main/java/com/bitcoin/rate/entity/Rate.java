package com.bitcoin.rate.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "BITCOIN_RATE")
public class Rate {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name="RATE", nullable = false)
    private BigDecimal rate;

    /** Always stored to DB in UTC zone */
    @Column(name = "_TIMESTAMP", nullable = false)
    private LocalDateTime timestamp;
}
