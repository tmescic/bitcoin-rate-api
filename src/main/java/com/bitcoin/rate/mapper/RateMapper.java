package com.bitcoin.rate.mapper;

import com.bitcoin.rate.dto.RateDTO;
import com.bitcoin.rate.entity.Rate;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Used for mapping between JPA entities and DTOs.
 */
public class RateMapper {

    public static RateDTO toDto(Rate rate) {
        return new RateDTO(rate.getId(), rate.getRate(), rate.getTimestamp().atZone(ZoneId.of("UTC")));
    }

    public static List<RateDTO> toDtoList(List<Rate> rates) {
        return rates.stream().map(r -> toDto(r)).collect(Collectors.toList());
    }

}
