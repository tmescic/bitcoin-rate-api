package com.bitcoin.rate.dto;

import lombok.Data;

/**
 * This is the object that is returned by teh CEX.IO API for fetching
 * latest exchange rates.
 */
@Data
public class CexResponseDTO {

    private String lprice;
    private String curr1;
    private String curr2;

}
