package com.bitcoin.rate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
public class RateDTO {

    private Long id;
    private BigDecimal rate;
    private ZonedDateTime timestamp;
}
