package com.bitcoin.rate.jobs;

import com.bitcoin.rate.entity.Rate;
import com.bitcoin.rate.repo.RateRepository;
import com.bitcoin.rate.dto.CexResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Background job used for fetch the latest BTC/USD exchange rate from a public API endpoint
 * and store it to the DB. It runs on a fixed schedule, the rate can be changed via the
 * 'fetch.rate.interval.millis' configuration property.
 */
@Component
public class RateFetcher {

    private static final Logger logger = LoggerFactory.getLogger(RateFetcher.class);

    @Value("${cex.io.endpoint}")
    private String cexIoEndpoint;

    private final RestTemplate restTemplate;
    private final RateRepository rateRepository;

    public RateFetcher(RestTemplate restTemplate, RateRepository rateRepository) {
        this.restTemplate = restTemplate;
        this.rateRepository = rateRepository;
    }

    @Scheduled(fixedRateString = "${fetch.rate.interval.millis}")
    public void fetchNewRate() {

        logger.debug("Fetching latest BTC / USD rate from : {}", cexIoEndpoint);

        CexResponseDTO latestPrice = restTemplate.getForObject(cexIoEndpoint, CexResponseDTO.class);

        storeToDB(latestPrice);
    }

    @Transactional
    public void storeToDB(CexResponseDTO latestPrice) {
        Rate newRate = new Rate();
        newRate.setRate(new BigDecimal(latestPrice.getLprice()));
        newRate.setTimestamp(LocalDateTime.now(ZoneId.of("UTC")));

        rateRepository.save(newRate);

        logger.info("New rate was stored to the DB: {}", newRate);
    }
}
