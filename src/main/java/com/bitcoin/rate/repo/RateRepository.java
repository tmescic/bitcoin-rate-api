package com.bitcoin.rate.repo;

import com.bitcoin.rate.entity.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


public interface RateRepository extends JpaRepository<Rate, Long> {

    @Query("select r from Rate r where r.timestamp = (select max(rr.timestamp) from Rate rr)")
    Optional<Rate> getLatestRate();

    List<Rate> findByTimestampBetweenOrderByTimestampAsc(LocalDateTime from, LocalDateTime to);
}