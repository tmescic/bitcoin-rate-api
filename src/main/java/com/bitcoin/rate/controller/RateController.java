package com.bitcoin.rate.controller;


import com.bitcoin.rate.entity.Rate;
import com.bitcoin.rate.repo.RateRepository;
import com.bitcoin.rate.dto.RateDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static com.bitcoin.rate.mapper.RateMapper.toDto;
import static com.bitcoin.rate.mapper.RateMapper.toDtoList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Public API exposing endpoints to fetch BTC/USD exchange rate.
 */
@RestController
@RequestMapping("/api/v1")
public class RateController {

    private static final Logger logger = LoggerFactory.getLogger(RateController.class);

    private RateRepository rateRepository;

    public RateController(RateRepository rateRepository) {
        this.rateRepository = rateRepository;
    }

    /**
     * @return the latest BTC/USD exchange rate from the DB.
     */
    @GetMapping("/latest-rate")
    public RateDTO getLatestRate() {

        logger.debug("Fetching latest BTC/USD exchange rate.");

        Optional<Rate> latestRate = rateRepository.getLatestRate();

        if (latestRate.isEmpty()) {
            logger.warn("No rates found in the DB.");
            throw new ResponseStatusException(NOT_FOUND, "No BTC/USD rates found in the system.");
        } else {
            RateDTO rateDTO = toDto(latestRate.get());
            logger.debug("Returning latest BTC/USD exchange rate");
            return rateDTO;
        }
    }

    /**
     * Returns all exchange rates in the specified period (from - to). Both parameters and mandatory,
     * and them must be in ISO 8601 datetime format. List is sorted by timestamp.
     */
    @GetMapping("/rate")
    public List<RateDTO> getRatesBetween(
            @RequestParam("from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime from,
            @RequestParam("to")   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime to) {

        LocalDateTime fromUTC = from.withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();
        LocalDateTime toUTC   = to.withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime();

        if (to.isBefore(from)) {
            throw new ResponseStatusException(BAD_REQUEST, "End time cannot be before start time.");
        }

        List<Rate> rates = rateRepository.findByTimestampBetweenOrderByTimestampAsc(fromUTC, toUTC);

        return toDtoList(rates);
    }
}
