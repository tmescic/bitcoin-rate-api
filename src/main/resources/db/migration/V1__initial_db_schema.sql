-- create initial DB schema

CREATE TABLE BITCOIN_RATE (
  ID INT AUTO_INCREMENT  PRIMARY KEY,
  RATE DECIMAL(20, 6) NOT NULL,
  _TIMESTAMP TIMESTAMP NOT NULL   -- will always be stored as UTC
);

CREATE UNIQUE INDEX PK_UNIQUE_TIME_FETCHED ON BITCOIN_RATE ( _TIMESTAMP  );
