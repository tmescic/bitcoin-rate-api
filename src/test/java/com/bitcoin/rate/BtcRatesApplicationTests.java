package com.bitcoin.rate;

import com.bitcoin.rate.controller.RateController;
import com.bitcoin.rate.dto.RateDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class BtcRatesApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private RateController controller;

	/** Sanity check test */
	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

	/** Fetches the latest exchange rate.
	 * (check test data in src/test/resources/data.sql) */
	@Test
	public void latestRate_returnsCorrectRate()  {
		RateDTO rate = this.restTemplate.getForObject("http://localhost:" + port + "/api/v1/latest-rate", RateDTO.class);
		assertThat(rate.getRate()).isEqualByComparingTo(new BigDecimal("9200"));
		assertThat(rate.getTimestamp()).isEqualTo(ZonedDateTime.of(LocalDateTime.parse("9999-06-21T22:00:00.000"), ZoneId.of("UTC")));
		assertThat(rate.getId()).isEqualTo(1002);
	}

	/** Invalid input test (time from must be before time to) */
	@Test
	public void timeFromIsAfterTimeTo_returns400BadRequest() {
		ResponseEntity response = this.restTemplate.getForEntity(
				"http://localhost:" + port + "/api/v1/rate?from=9999-06-21T22:00:00.000Z&to=9999-06-20T22:00:00.000Z",
				String.class);
		assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
	}


	/** Invalid input test (missing query params) */
	@Test
	public void missingQueryParams_returns400BadRequest() {
		ResponseEntity response = this.restTemplate.getForEntity(
				"http://localhost:" + port + "/api/v1/rate",
				String.class);
		assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
	}

	/** Invalid input test (timestamp not in ISO 8601 format - missing timezone) */
	@Test
	public void wrongTimeFormat_returns400BadRequest() throws URISyntaxException {
		ResponseEntity response = this.restTemplate.getForEntity(
				"http://localhost:" + port + "/api/v1/rate?from=9999-06-21T22:00:00.000&to=9999-06-20T22:00:00.000",
				String.class);
		assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.BAD_REQUEST);
	}

	/** Fetch rates in period:
	 *      from: 9999-06-20T19:00:00.000Z
	 *      to:   9999-06-21T16:00:00.000-05:00
	 *   (check test data in src/test/resources/data.sql)
	 */
	@Test
	public void fetchHistoryicRates_returnsCorrectData() throws URISyntaxException, JsonProcessingException {

		ResponseEntity<RateDTO[]> response =
				restTemplate.getForEntity(
						"http://localhost:" + port + "/api/v1/rate?from=9999-06-20T19:00:00.000Z&to=9999-06-21T16:00:00.000-05:00",
						RateDTO[].class);
		RateDTO[] rates = response.getBody();

		assertThat(rates.length).isEqualTo(3);

		// rates are sorted by timestamp

		assertThat(rates[0].getRate()).isEqualByComparingTo(new BigDecimal("9100"));
		assertThat(rates[0].getTimestamp()).isEqualTo(ZonedDateTime.of(LocalDateTime.parse("9999-06-20T21:00:00.000"), ZoneId.of("UTC")));
		assertThat(rates[0].getId()).isEqualTo(1001);

		assertThat(rates[1].getRate()).isEqualByComparingTo(new BigDecimal("9300"));
		assertThat(rates[1].getTimestamp()).isEqualTo(ZonedDateTime.of(LocalDateTime.parse("9999-06-21T20:00:00.000"), ZoneId.of("UTC")));
		assertThat(rates[1].getId()).isEqualTo(1003);

		assertThat(rates[2].getRate()).isEqualByComparingTo(new BigDecimal("9500"));
		assertThat(rates[2].getTimestamp()).isEqualTo(ZonedDateTime.of(LocalDateTime.parse("9999-06-21T21:00:00.000"), ZoneId.of("UTC")));
		assertThat(rates[2].getId()).isEqualTo(1005);
	}
}
