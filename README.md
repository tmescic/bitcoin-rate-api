# Bitcoin exchange rate API

#### Requirements
 - Java 11
 
#### Only run tests
 `./mvnw test`
 
 Expected output: `Tests run: 6, Failures: 0, Errors: 0, Skipped: 0`

#### Build and run the app

`./mvnw clean install`

`java -jar target/bitcoin-0.0.1-SNAPSHOT.jar`
(uses default fetch period of 5 seconds)

`java -jar -Dfetch.rate.interval.millis=1000 target/bitcoin-0.0.1-SNAPSHOT.jar`

#### Configuration params

Can be set in `application.properties` or via a command line switch.

```
# Interval for fetching the exchange rate (in milliseconds)
fetch.rate.interval.millis=5000

# the API endpoint that will be called to get the latest rate/price of 1 BTC in dollars
cex.io.endpoint=https://cex.io/api/last_price/BTC/USD

# timeouts to set when calling REST APIs
rest.call.read.timeout.millis=1000
rest.call.connect.timeout.millis=1000
```

##### Get latest rate endpoint

curl "http://localhost:8080/api/v1/latest-rate"

Output:
```
{
   "id":288,
   "rate":9423.100000,
   "timestamp":"2020-06-18T18:54:02.701326Z"
}
```

##### Get historic rates

curl "http://localhost:8080/api/v1/rate?from=2020-06-21T22:00:00.000Z&to=2020-06-25T22:00:00.000+02:00"

Timestamp must be in ISO 8601 format.

Output:
```
[
   {
      "id":223,
      "rate":9418.700000,
      "timestamp":"2020-06-18T18:48:37.689244Z"
   },
   {
      "id":224,
      "rate":9418.700000,
      "timestamp":"2020-06-18T18:48:42.707093Z"
   },
.....
   {
      "id":231,
      "rate":9418.700000,
      "timestamp":"2020-06-18T18:49:17.699481Z"
   }
]
```
